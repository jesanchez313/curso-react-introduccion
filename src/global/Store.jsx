import React, { useReducer } from "react";

const initialState = {
  name: "Jeferson",
  age: 26
};

export const Store = React.createContext(initialState);

export const reducer = (state, action) => {
  switch (action.type) {
    case "INC_AGE":
      return { ...state, age: state.age + 1 };
    case "CHANGE_NAME":
      return { ...state, name: action.payload.name };
    default:
      return state;
  }
};

export const StoreProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <Store.Provider value={{ state, dispatch }}>{children}</Store.Provider>
  );
};
