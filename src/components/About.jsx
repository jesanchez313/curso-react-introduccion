import React from "react";
import { Link } from "react-router-dom";

export default class About extends React.Component {
  componentWillUnmount() {
    alert("You will leave about page");
  }
  render() {
    return (
      <div>
        <h2>About</h2>
        <Link to="/">
          <button>go to home</button>
        </Link>
      </div>
    );
  }
}
