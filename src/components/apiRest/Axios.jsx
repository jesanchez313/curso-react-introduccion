import React from "react";
import { useGetData } from "use-axios-react";

export default function Axios() {
  const [data, loading] = useGetData(
    "https://rickandmortyapi.com/api/character/"
  );

  if (loading) return <p>Loading...</p>;

  return (
    <div>
      <h2>Rick and morty - character - with axios</h2>
      {loading ? <h2>Loading...</h2> : ""}
      <div>
        {data.results.map((character, index) => (
          <div key={index}>
            <img src={character.image} alt={character.name} />
            <div>{character.name}</div>
          </div>
        ))}
      </div>
    </div>
  );
}
