import React, { useEffect, useState } from "react";

export default function Fetch() {
  const [characters, setCharacters] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    fetch("https://rickandmortyapi.com/api/character/")
      .then(response => response.json())
      .then(data => {
        setCharacters(data.results);
        setLoading(false);
      });
  }, []);

  return (
    <div>
      <h2>Rick and morty - character</h2>
      {loading ? <h2>Loading...</h2> : ""}
      <div>
        {characters.map((character, index) => (
          <div key={index}>
            <img src={character.image} alt={character.name} />
            <div>{character.name}</div>
          </div>
        ))}
      </div>
    </div>
  );
}
