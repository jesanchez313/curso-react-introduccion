import React from "react";
import { useParams } from "react-router-dom";

export default function Blog() {
  const { title, date } = useParams();
  return (
    <div>
      This is a blog with title {title}, with date {date}
    </div>
  );
}
