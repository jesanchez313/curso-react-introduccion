import React from "react";

const MiBoton = props => {
  const styles = { background: "blue", color: "white" };

  return <button {...props} style={styles} />;
};

export default MiBoton;
