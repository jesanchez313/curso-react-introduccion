import React from "react";

class MiBoton extends React.Component {
  constructor(props) {
    super(props);
    this.state.style = { background: "blue", color: "white" };
  }
  render() {
    return <button {...this.props} style={this.state.styles} />;
  }
}

export default MiBoton;
