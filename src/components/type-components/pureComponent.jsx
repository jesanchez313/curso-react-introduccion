import React from "react";

class MiBoton extends React.PureComponent {
  styles = { background: "blue", color: "white" };

  render() {
    return <button {...this.props} style={this.styles} />;
  }
}

export default MiBoton;
