import React from "react";

const TruncateHOC = WrappedComponent =>
  class extends React.Component {
    static propTypes = {
      text: React.PropTypes.string.isRequired
    };

    truncateText(text) {
      const maxLength = 25;

      if (text.length > maxLength) {
        return `${text.substring(0, maxLength - 3)}...`;
      } else {
        return text;
      }
    }

    // Renderizamos el componente
    render() {
      // Creamos una copia de los props actuales
      let props = Object.assign({}, this.props);
      // Modificamos el texto
      props.text = this.truncateText(props.text);

      // Renderizamos el componente principal
      return <WrappedComponent {...props} />;
    }
  };

export default TruncateHOC;

let Description = props => <p>{props.text}</p>;

class Article extends React.Component {
  render() {
    return <article>{this.props.text}</article>;
  }
}

// Se aplica el HOC!
Description = TruncateHOC(Description);
Article = TruncateHOC(Article);
