import React from "react";

export default function GooglePage() {
  return (
    <div>
      <Logo />
      <WordBox />
      <SearchButton />
      <LuckyButton />
      <LanguageNote />
    </div>
  );
}

export function Logo() {
  return <div>logo</div>;
}

export function WordBox() {
  return <div>logo</div>;
}

export function SearchButton() {
  return <div>logo</div>;
}

export function LuckyButton() {
  return <div>logo</div>;
}

export function LanguageNote() {
  return <div>logo</div>;
}
