import React from "react";

export default function Hello({
  name,
  favoriteSport = ["Futbol", "VideoGames"],
  children
}) {
  return (
    <>
      <div>Welcome to course : {name}</div>
      <ul>
        {favoriteSport.map((sport, index) => (
          <option key={index} value={sport}>
            {sport}
          </option>
        ))}
      </ul>
      <div>
        Child Component:
        {children}
      </div>
    </>
  );
}
