import React, { useContext, useState } from "react";
import { Store } from "../global/Store";

export default function GlobalState() {
  const {
    state: { name: nameContext, age },
    dispatch
  } = useContext(Store);
  const [name, setName] = useState(nameContext);

  const handleIncrementAge = () => {
    dispatch({
      type: "INC_AGE"
    });
  };

  const handleName = () => {
    dispatch({
      type: "CHANGE_NAME",
      payload: {
        name
      }
    });
  };

  return (
    <div>
      <p>name: {nameContext}</p>
      <p>age: {age}</p>
      <button onClick={handleIncrementAge}>Increment age</button>
      <div>
        <input
          type="text"
          onChange={e => setName(e.target.value)}
          value={name}
        />
      </div>
      <button onClick={handleName}>Change name</button>
    </div>
  );
}
