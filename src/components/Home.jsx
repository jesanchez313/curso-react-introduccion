import React from "react";
import Hello from "./properties/Hello";
import ClassComponent from "./states/ClassComponent";
import Modal from "./Modal";
import logo from "../logo.svg";
import { Link } from "react-router-dom";
import Fetch from "./apiRest/Fetch";
import Axios from "./apiRest/Axios";

export default function Home() {
  return (
    <div>
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      <h2>Properties:</h2>
      <Hello name="Jeferson Sanchez"></Hello>
      <Hello name="Esteban Buritica" favoriteSport={["Basketball"]}>
        <h1>Another Element</h1>
      </Hello>
      <hr></hr>
      <h2>States:</h2>
      <ClassComponent></ClassComponent>
      <Modal>Modal</Modal>
      <hr></hr>
      <h2>Pages</h2>
      <Link to="/about">
        <button>Go to about</button>
      </Link>
      <hr></hr>
      {/* <Fetch></Fetch> */}
      <hr></hr>
      <Axios></Axios>
    </div>
  );
}
