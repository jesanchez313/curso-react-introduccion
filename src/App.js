import React from "react";
import "./App.css";
import Home from "./components/Home";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import About from "./components/About";
import NotFound from "./components/NotFound";
import Blog from "./components/Blog";
import GlobalState from "./components/GlobalState";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/" exact component={Home}></Route>
          <Route path="/about/" exact component={About}></Route>
          <Route path="/blog/:title/:date" exact component={Blog}></Route>
          <Route path="/info/" exact component={GlobalState}></Route>
          <Route component={NotFound}></Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
