import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { StoreProvider } from "./global/Store";
import * as serviceWorker from "./serviceWorker";

/**
|--------------------------------------------------
| Que es jsx y como funciona
|--------------------------------------------------
*/
// const hello = React.createElement("h1", {}, "Curso react js");
// const helloJsx = <h1>Curso react js - JSX</h1>;

// const lista = React.createElement(
//   "ul",
//   { class: "nav" },
//   React.createElement("option", {}, "Opcion 1"),
//   React.createElement("option", {}, "Opcion 2")
// );
// const listaJsx = (
//   <ul className="nav">
//     <option>Opcion 1 jsx</option>
//     <option>Opcion 2 jsx</option>
//   </ul>
// );

// const nombre = "Curso React";
// const curso = React.createElement("p", {}, nombre);
// const cursoJsx = <p>{nombre} jsx</p>;

ReactDOM.render(
  <StoreProvider>
    <App></App>
  </StoreProvider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
